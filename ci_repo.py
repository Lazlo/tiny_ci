import subprocess

class CiRepo():

    @staticmethod
    def get_repo_dirname_from_url(url):
        last_slash_pos = url.rindex("/")
        dirname = url[last_slash_pos + 1:]
        dirname = dirname.replace(".git", "")
        return dirname

    @staticmethod
    def run_cmd(cmd, cwd=None):
        p = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE)
        return p.communicate()

    def __init__(self, repo_url):
        self.url = repo_url
        self.dirname = self.get_repo_dirname_from_url(self.url)

    def clone(self):
        cmd = ["git", "clone", self.url]
        subprocess.call(cmd)

    def remote_changed(self):
        cmd = ["git", "remote", "update"]
        self.run_cmd(cmd, self.dirname)
        cmd = ["git", "status", "-uno"]
        out, err = self.run_cmd(cmd, self.dirname)
        # FIXME This is so super ugly as we compare localized strings!
        # Think about using python-git bindings!
        if not "Your branch is up to date" in out:
            return True
        return False

    def pull(self):
        cmd = ["git", "pull", "origin", "master"]
        subprocess.call(cmd, cwd=self.dirname)
