# depends on python-yaml package

import sys
import os
import yaml
#import shlex
import subprocess

class BuildCfg():

	DEFAULT_BUILD_YML		= "travis.yml"

	E_MSG_FILE_NOT_FOUND		= "ERROR: %s does not exist!\n"
	E_MSG_SECTION_KEY_NOT_FOUND	= "ERROR: no %s section found in %s!\n"	
	E_MSG_NONZERO_EXIT		= "ERROR: %s exited with %d\n"

	SECTION_KEYS			= [
						{"name": "before_install",	"required": False},
						{"name": "install",		"required": False},
						{"name": "before_script",	"required": False},
						{"name": "script",		"required": True},
						{"name": "after_script",	"required": False}
					]

	def __init__(self, build_yml=None):
		self.build_yml = self.DEFAULT_BUILD_YML
		if build_yml:
			self.build_yml = build_yml
		self.build_cfg = None

	def _section_exists(self, section_key):
		if section_key in self.build_cfg:
			return True
		return False

	def parse(self):
		if not os.path.exists(self.build_yml):
			sys.stderr.write(self.E_MSG_FILE_NOT_FOUND % self.build_yml)
			return False
		self.build_cfg = yaml.load(open(self.build_yml).read())
		# FIXME take required section key from class variable
		if not self._section_exists("script"):
			sys.stderr.write(self.E_MSG_SECTION_KEY_NOT_FOUND % ("script", self.build_yml))
			return False
                return True

	def _run_section(self, section_key):
		for cmd in self.build_cfg[section_key]:
			# FIXME we are supposed to slipt a command by spaced, preferably using shlex
			# But we noticed for example 'echo "XXX"' will not work as expected (output
			# is not shown) or 'ls -1 /tmp' will give the same result as 'ls -1' (as if
			# there was not path argument).
			#cmd = shlex.split(cmd)
			#print("cmd = %s" % cmd)
			rc = subprocess.call(cmd, shell=True)
			if rc != 0:
				sys.stderr.write(self.E_MSG_NONZERO_EXIT % (cmd.split(" ")[0], rc))
				return False
                return True

	def run(self):
		for section in self.SECTION_KEYS:
			# exit if required section is missing
			if not self._section_exists(section["name"]) and section["required"]:
				sys.stderr.write(self.E_MSG_SECTION_KEY_NOT_FOUND % (section["name"], self.build_yml))
				sys.exit(1)
			# skip optional sections
			if not self._section_exists(section["name"]):
				continue
			self._run_section(section["name"])
