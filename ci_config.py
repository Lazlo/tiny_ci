from ci_repo import CiRepo

class CiConfig():

    def __init__(self):
        self.repo_watch_list = []

    def add(self, repo_url):
        self.repo_watch_list.append(CiRepo(repo_url))
