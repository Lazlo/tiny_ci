#!/usr/bin/env python

import sys
import time
import os

from ci_config import CiConfig
from ci_build import BuildCfg

def log(msg):
    sys.stdout.write("%s\n" % msg)

class CiDaemon():

    DEFAULT_SLEEP_DURATION_SEC = 1

    def __init__(self, cfg=None):
        self.cfg = cfg

    def _startup(self):
        log("starting up")
        for repo in self.cfg.repo_watch_list:
            print(repo.__dict__)
            if os.path.exists(repo.dirname):
                continue
            repo.clone()

    def _idle(self):
        time.sleep(self.DEFAULT_SLEEP_DURATION_SEC)

    def _shutdown(self):
        log("shutting down")

    def run(self):
        try:
            self._startup()
            while True:
                log("tick")
                for repo in self.cfg.repo_watch_list:
                    if not repo.remote_changed():
                        continue
                    print("remote changed!")
                    repo.pull()
                    # TODO Schedule for build!
                self._idle()
        except KeyboardInterrupt:
            self._shutdown()

if __name__ == "__main__":
    d = CiDaemon(CiConfig())
    d.run()
