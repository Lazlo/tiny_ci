# Tiny CI

Is a minimalist continuous integration daemon written in Python.

It will track changes in remote git repositories, fetch updates if there was a change, parse and finally execute commands listed in a .travis.yml file that must be contained in the repository of the project.
